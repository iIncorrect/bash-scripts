#!/bin/bash -x

. ./dump_config.sh

##
while [[ -n "$1" ]]; do
  param=$1
  shift
  case $param in
    --dump_mongo)
      dump_mongo=$1
      shift
      ;;
    --dump_tables)
      dump_all_tables=$1
      shift
      ;;
  esac
done
##

time_hash=`date +%F---%H-%M`
dir="/tmp/$time_hash"

db_ok=$(mysql -h$mysqlhost -u$db_user -p$db_pass $db_name -e "SHOW DATABASES;" 2> /dev/null | grep $db_name)

if [[ "$db_ok" != "$db_name" ]]; then
  echo "ERROR: check parameters"
  exit -1
fi

[[ -d $dir ]] && rm -rf $dir
mkdir -p $dir

if [[ "$dump_all_tables" == "0" ]]; then
  # find co traces table name
  traces_table_name=$(mysql -h$mysqlhost -u$db_user -p$db_pass $db_name -e \
  "SELECT CONCAT(\"co_records_\", (select \`custom_objects\`.\`id\`
FROM \`${db_name}\`.\`custom_objects\`
WHERE \`custom_objects\`.\`alias\` = \"traces\")) as \"table_name\"
LIMIT 1 \G" 2> /dev/null |\
  tail -1 |\
  awk '{print $2}')

  # define ignored tables
  [[ "$traces_table_name" == "NULL" ]] || ignored_tables+="
  $traces_table_name"

  for ignored_table in $ignored_tables; do
    ignored_tables_string+=" --ignore_table=${db_name}.${ignored_table}"
  done
else
  ignored_tables_string=''
  ignored_tables=''
fi

# dump mysql database without ignored tables
mysqldump \
  -h$mysqlhost \
  -u$db_user \
  -p$db_pass \
  $db_name \
  ${ignored_tables_string} > $dir/${time_hash}.${hostname}.${db_name}.mysql.sql 2> /dev/null &

cd $dir

if [[ "$dump_mongo" == "1" ]]; then
 mongodump \
  --host $mongohost \
  --username $db_user \
  --password $db_pass \
  --db $db_name > /dev/null
 rm $dir/dump/$db_name/system*
fi
wait

# dump ignored tables structure
for ignored_table in $ignored_tables; do
  mysqldump \
    -h$mysqlhost \
    -u$db_user \
    -p$db_pass \
    $db_name \
    ${ignored_table} \
    --no-data >> $dir/$time_hash.${hostname}.$db_name.mysql.sql 2> /dev/null
done

tar -cf $dir/$time_hash.${hostname}.$db_name.tar $dir/* 2> /dev/null
bzip2 -c < $dir/$time_hash.${hostname}.$db_name.tar > $dir/$time_hash.${hostname}.$db_name.tar.bz2
rm $dir/$time_hash.${hostname}.$db_name.tar
rm $dir/$time_hash.${hostname}.$db_name.mysql.sql
cp $dir/$time_hash.${hostname}.$db_name.tar.bz2 $path_to_public
echo "$portal_url$time_hash.${hostname}.$db_name.tar.bz2"
rm -rf $dir
