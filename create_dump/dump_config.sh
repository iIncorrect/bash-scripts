#!/bin/bash

mongohost='127.0.0.1'
mysqlhost='127.0.0.1'
db_user='deploy'
db_pass='deploypass'
db_name='deploy_production'

logsdb_host=''
logsdb_user=''
logsdb_pass=''
logsdb_name=''
logsdb_key=''
logsdb_cert=''

path_to_public="/var/www/streamline/current/public/"
portal_url="http://url/"
dump_mongo=1
dump_all_tables=0
ignored_tables_string=''
ignored_tables="full_package_logs
logs"
hostname=$(hostname)

